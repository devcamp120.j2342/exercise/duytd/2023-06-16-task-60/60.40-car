package com.devcamp.task6040;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaCarCartypeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaCarCartypeApplication.class, args);
	}

}
