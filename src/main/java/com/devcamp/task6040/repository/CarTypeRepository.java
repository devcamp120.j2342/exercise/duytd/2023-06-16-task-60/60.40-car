package com.devcamp.task6040.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6040.model.CarType;

public interface CarTypeRepository extends JpaRepository<CarType, Long> {
    
}
